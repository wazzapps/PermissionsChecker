package com.wazzapps.wrapper.permissions;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import static com.unity3d.player.UnityPlayer.currentActivity;

    public class PermissionsChecker {
    private static final int REQUEST_PERMISSION_CODE = 32100;
    private static final String TAG = "PermissionsChecker";

    private static final String PERMISSION_GRANTED = "granted";
    private static final String PERMISSION_DENIED = "denied";

    private static String callbackGameObjectName = "";
    private static String callbackMethodName = "";

    private static Fragment requestFragment;

    public static boolean checkPermission(String permission) {
        int permissionCheck = PackageManager.PERMISSION_GRANTED;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissionCheck = currentActivity.checkSelfPermission(permission);
        }

        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean shouldShowPermissionRationale(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return currentActivity.shouldShowRequestPermissionRationale(permission);
        } else {
            return false;
        }
    }

    public static void askForPermission(String permission, String gameObjectName, String methodName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionsChecker.callbackGameObjectName = gameObjectName;
            PermissionsChecker.callbackMethodName = methodName;

            final String permissionName = permission;

            try {
                final FragmentManager fragmentManager = UnityPlayer.currentActivity.getFragmentManager();
                PermissionsChecker.requestFragment = new Fragment() {

                    @Override public void onStart()
                    {
                        super.onStart();
                        String[] permissionsToRequest = new String [] { permissionName };
                        Log.i(TAG,"fragment start " + permissionsToRequest[0]);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsToRequest, REQUEST_PERMISSION_CODE);
                        }
                    }

                    @Override public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
                    {
                        Log.i(TAG, "onRequestPermissionsResult");
                        if (requestCode != REQUEST_PERMISSION_CODE) {
                            return;
                        }

                        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            Log.i(TAG, PERMISSION_GRANTED + permissions[0]);
                            UnityPlayer.UnitySendMessage(PermissionsChecker.callbackGameObjectName, PermissionsChecker.callbackMethodName, PERMISSION_GRANTED + permissions[0]);
                        } else {
                            Log.i(TAG, PERMISSION_DENIED + permissions[0]);
                            UnityPlayer.UnitySendMessage(PermissionsChecker.callbackGameObjectName, PermissionsChecker.callbackMethodName, PERMISSION_DENIED + permissions[0]);
                        }

                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.remove(this);
                        fragmentTransaction.commit();
                    }
                };

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(0, PermissionsChecker.requestFragment);
                fragmentTransaction.commit();

            } catch (Exception e) {
                Log.e(TAG, "Unable to start request for permission");
            }
        } else {
            Log.i(TAG, PERMISSION_GRANTED + permission);
            UnityPlayer.UnitySendMessage(gameObjectName, methodName, PERMISSION_GRANTED + permission);
        }
    }
}