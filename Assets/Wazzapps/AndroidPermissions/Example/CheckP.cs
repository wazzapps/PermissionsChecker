﻿using UnityEngine;
using Wazzapps;

public class CheckP : MonoBehaviour
{
    public void Check()
    {
        PermissionsChecker.Instance.OnPermissionGranted += OnPermissionGranted;
        PermissionsChecker.Instance.OnPermissionDenied += OnPermissionDenied;

        if (PermissionsChecker.Instance.CheckPermission(PermissionName.WRITE_EXTERNAL_STORAGE) == false)
        {
            PermissionsChecker.Instance.AskForPermission(PermissionName.WRITE_EXTERNAL_STORAGE);
        } else {
            Debug.Log("Already granted!");
        }
    }

    void OnPermissionGranted(string permission)
    {
        Debug.Log("OnPermissionGranted " + permission);
    }

    void OnPermissionDenied(string permission)
    {
        Debug.Log("OnPermissionDenied " + permission);
    }
}
