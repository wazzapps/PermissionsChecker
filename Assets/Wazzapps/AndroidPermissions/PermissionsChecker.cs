﻿using UnityEngine;
namespace Wazzapps
{
    public class PermissionsChecker : MonoBehaviour
    {
        private const string CALLBACK_GAME_OBJECT_NAME = "PermissionsChecker";
        private const string CALLBACK_METHOD_NAME = "OnRequestPermissionsResult";
        private const string PERMISSION_GRANTED = "granted";
        private const string PERMISSION_DENIED = "denied";

        public delegate void OnPermissionGrantedDelegate(string permission);
        public delegate void OnPermissionDeniedDelegate(string permission);
        public OnPermissionGrantedDelegate OnPermissionGranted;
        public OnPermissionDeniedDelegate OnPermissionDenied;

        private static PermissionsChecker _instance;
        public static PermissionsChecker Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<PermissionsChecker>();
                    if (_instance != null)
                    {
                        _instance.gameObject.name = CALLBACK_GAME_OBJECT_NAME;
                    }
                }
                if (_instance == null)
                {
                    GameObject go = new GameObject(CALLBACK_GAME_OBJECT_NAME);
                    _instance = go.AddComponent<PermissionsChecker>();
                }
                return _instance;
            }
        }

        void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }

#if UNITY_ANDROID && !UNITY_EDITOR
        private AndroidJavaClass _permissionsWrapper;
#endif

        public bool CheckPermission(string permission)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            return GetWrapper().CallStatic<bool>("checkPermission", permission);
#else
            return true;
#endif
        }

        public bool ShouldShowPermissionRationale(string permission)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            return GetWrapper().CallStatic<bool>("shouldShowPermissionRationale", permission);
#else
            return false;
#endif
        }

        public void AskForPermission(string permission)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            GetWrapper().CallStatic("askForPermission", permission, CALLBACK_GAME_OBJECT_NAME, CALLBACK_METHOD_NAME);
#else
            if (OnPermissionGranted != null)
            {
                OnPermissionGranted(permission);
            }
#endif
        }

#if UNITY_ANDROID && !UNITY_EDITOR
        private AndroidJavaClass GetWrapper()
        {
            if (_permissionsWrapper == null)
            {
                _permissionsWrapper = new AndroidJavaClass("com.wazzapps.wrapper.permissions.PermissionsChecker");
            }
            return _permissionsWrapper;
        }

        public void OnRequestPermissionsResult(string result)
        {
            if (result.StartsWith(PERMISSION_GRANTED))
            {
                if (OnPermissionGranted != null)
                {
                    OnPermissionGranted(result.Substring(PERMISSION_GRANTED.Length));
                }
            }
            else
            {
                if (OnPermissionDenied != null)
                {
                    OnPermissionDenied(result.Substring(PERMISSION_DENIED.Length));
                }
            }
        }
#endif

    }
}