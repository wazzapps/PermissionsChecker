# Библиотека для работы с PERMISSIONS в Android 6 (SDK 23) и выше #

Если в target SDK проекта указана версия Android SDK 23 и выше, то необходимо грамотно работать с разрешиниями, так как Unity их запрашивает на старте приложения, что отпугивает пользователей.
Эта библиотека отключает автоматический запрос разрешений, предоставляет API для проверки разрешений и их запроса. Внутри редактора Unity разрешения считаются всегда полученными.

Работа ведется через класс ```Wazzapps.PermissionsChecker```. В классе ```Wazzapps.PermissionName``` перечислены все возможные разрешения для Android.

## Проверка разрешения ##
Перед использованием функций, требующих разрешений необходимо проверить, предоставил ли это разрешение пользователь. Для этого необходимо вызвать метод CheckPermission:
```
bool result = PermissionsChecker.Instance.CheckPermission(PermissionName.WRITE_EXTERNAL_STORAGE);
```

## Проверка, необходимо ли показывать окно с описанием, для чего нужно разрешение ##
Android SDK предоставляет метод, подсказывающий, нужно ли показывать информацию, для чего нужно разрешение (непосредственно перед запросом разрешения).
```
if(PermissionsChecker.Instance.ShouldShowPermissionRationale(PermissionName.WRITE_EXTERNAL_STORAGE)) { ... }
```

## Запрос разрешения ##
Если при проверке CheckPermission оказалось, что разрешение не получено, необходимо показать окно запроса. Для получения каллбэка после того, как пользователь предоставит или отзовет разрешения необходимо подписаться на делегат OnPermissionGranted и OnPermissionDenied.
**Важно! Необходимо проверить имя разрешения, которое передается в делегат!**
```
PermissionsChecker.Instance.OnPermissionGranted += (string permission) => { Debug.Log("OnPermissionGranted " + permission); };
PermissionsChecker.Instance.OnPermissionDenied += (string permission) => { Debug.Log("OnPermissionDenied " + permission); };

if (PermissionsChecker.Instance.CheckPermission(PermissionName.WRITE_EXTERNAL_STORAGE) == false)
{
    PermissionsChecker.Instance.AskForPermission(PermissionName.WRITE_EXTERNAL_STORAGE);
} else {
    Debug.Log("Already granted!");
}
```